#!/bin/bash

source /vagrant/scripts/config.sh

apt-get update

apt-get install -y build-essential
apt-get install -y curl
apt-get install -y htop

# --- Java repository ---
export DEBIAN_FRONTEND=noninteractive
apt-get install -y python-software-properties software-properties-common
add-apt-repository ppa:webupd8team/java

# --- Mesosphere repository ---
apt-key adv --keyserver keyserver.ubuntu.com --recv E56151BF
DISTRO=$(lsb_release -is | tr '[:upper:]' '[:lower:]')
CODENAME=$(lsb_release -cs)
echo "deb http://repos.mesosphere.io/${DISTRO} ${CODENAME} main" | tee /etc/apt/sources.list.d/mesosphere.list

apt-get update

# --- Install Java ---
echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections
apt-get install -y oracle-java8-installer oracle-java8-set-default

# --- Mesos ---
apt-get install -y mesos

echo "zk://${MASTER_IP}:2181/mesos" | tee /etc/mesos/zk
